// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//  "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function problem1(val, inventory) {
    for (let i = 0; i < inventory.length; i++) {                                                             // Running the loop from 0->length of invertory -1
        if (inventory[i].id == parseInt(val)) {                                                                   // Check if the current index matches the specified value. 0 index based.
            return (`Car ${inventory[i].id } ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`)  // Formatted and Returned output according to the format  
        }
    }
    return 'inventory not found'                                                                             // Return the message if invertory not found.
}


module.exports = problem1;                                                                                    // Exports the sortCarNames function to be used in other files
