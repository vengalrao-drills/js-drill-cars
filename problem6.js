var BmwAudi = [];                                           // Initializing an empty array to store BMW and Audi cars

function BMWAndAudi(inventory) {
    for (let i = 0; i < inventory.length; i++) {
        // Checking if the car_make is either 'bmw' or 'audi' (case insensitive)
        if (inventory[i].car_make.toLowerCase() === 'bmw' || inventory[i].car_make.toLowerCase() === 'audi') {
            BmwAudi.push(inventory[i].car_model);           // Adding car models of BMW and Audi to the BmwAudi array
        }
    }
    return BmwAudi;                                         // Returning the array containing BMW and Audi car models
}

module.exports = BMWAndAudi;                                // Exports the function to retrieve BMW and Audi cars
