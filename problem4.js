// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

var carYears = [];                                   // Initializing an empty array to store car years

function allYears(inventory) {
    for (let i = 0; i < inventory.length; i++) {
        carYears.push(inventory[i].car_year);        // Iterates through the inventory to extract and add car years 
    }
    return carYears;                          // Returns an array containing car years
}

module.exports = allYears;                         // Exports the function to retrieve car years
