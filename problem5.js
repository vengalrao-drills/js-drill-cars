
var olderCars = [];                                 // Initializing an empty array to store cars older than 2000

function old_Cars(inventory) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_year < 2000) {
            olderCars.push(inventory[i].car_year);  // Adding cars older than 2000 to the olderCars array
        }
    }
    return olderCars;                               // Returning the array of cars older than 2000
}

module.exports = old_Cars;                          // Exports the function  
