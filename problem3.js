
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
var CarName = []

function sortCarNames(inventory) {
    for (let i = 0; i < inventory.length; i++) { // Running the loop from 0->length of invertory -1
        CarName.push(inventory[i].car_model)     // Here we are adding car names i.e car_model adding into the array
    }
    CarName.sort()                               // Sorting the car names
    return CarName;                              // Returning the sorted car names
}
module.exports = sortCarNames;                   // Exports the sortCarNames function to be used in other files