const allYears = require('../problem4')           // Imports the function to sortByYear names from 'problem4.js'
const inventory = require('../inventory/inventory')   // Importing the car inventory data from the 'inventory.js'

// problem 4
console.log(allYears(inventory))                       // Calls the sortByYear function with the inventory data