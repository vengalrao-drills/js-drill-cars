const inventory = require("../inventory/inventory");         // Imports the function to old_Cars names from 'problem6.js'
const BMWAndAudi = require("../problem6");                   // Importing the car inventory data from the 'inventory.js'

// problem 6
console.log(BMWAndAudi(inventory))                          // Calls the BMWAndAudi function with the inventory data