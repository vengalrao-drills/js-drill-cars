const sortCarNames = require('../problem3')           // Imports the function to sortCarNames names from 'problem3.js'
const inventory = require('../inventory/inventory')   // Importing the car inventory data from the 'inventory.js'

// problem 3
console.log(sortCarNames(inventory))                  // Calls the sortCarNames function with the inventory data